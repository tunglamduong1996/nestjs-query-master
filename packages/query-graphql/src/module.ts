import { Assembler, NestjsQueryCoreModule, Class } from '@nestjs-query/core';
import { DynamicModule, ForwardReference, Provider } from '@nestjs/common';
import { AutoResolverOpts, createAuthorizerProviders, createResolvers } from './providers';
import { ReadResolverOpts } from './resolvers';
import { defaultPubSub, pubSubToken, GraphQLPubSub } from './subscription';
import { PagingStrategies } from './types/query/paging';
// import chalk from 'chalk'
// import { performance } from 'perf_hooks'

export interface NestjsQueryGraphqlModuleOpts {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  imports: Array<Class<any> | DynamicModule | Promise<DynamicModule> | ForwardReference>;
  services?: Provider[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  assemblers?: Class<Assembler<any, any, any, any, any, any>>[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  resolvers: AutoResolverOpts<any, any, unknown, unknown, ReadResolverOpts<any>, PagingStrategies>[];
  pubSub?: Provider<GraphQLPubSub>;
}

export class NestjsQueryGraphQLModule {
  static forFeature(opts: NestjsQueryGraphqlModuleOpts): DynamicModule {
    //console.log(chalk.redBright('init NestjsQueryGraphQLModule.................'))
    // const startInit = performance.now();

    // const startTimeCoreModule = performance.now();
    const coreModule = NestjsQueryCoreModule.forFeature({
      assemblers: opts.assemblers,
      imports: opts.imports,
    });
    // const endTimeCoreModule = performance.now();
    //console.log(chalk.green(`coreModule Load Time: ${endTimeCoreModule - startTimeCoreModule}ms`));

    // const startTimePubSubProvider = performance.now();
    const pubSubProvider = opts.pubSub ?? this.defaultPubSubProvider();
    // const endTimePubSubProvider = performance.now();
    //console.log(chalk.yellow(`pubSubProvider Load Time: ${endTimePubSubProvider - startTimePubSubProvider}ms`));

    // const startTimeDTOClasses = performance.now();
    const DTOClasses = opts.resolvers.map((r) => r.DTOClass);
    // const endTimeDTOClasses = performance.now();
    //console.log(chalk.cyan(`DTOClasses Load Time: ${endTimeDTOClasses - startTimeDTOClasses}ms`));

    // const startTimeResolverProviders = performance.now();
    const resolverProviders = createResolvers(opts.resolvers);
    // const endTimeResolverProviders = performance.now();
    //console.log(chalk.magenta(`resolverProviders Load Time: ${endTimeResolverProviders - startTimeResolverProviders}ms`));

    // const startTimeProviders = performance.now();
    const providers = [...(opts.services || []), ...createAuthorizerProviders(DTOClasses)];
    // const endTimeProviders = performance.now();
    //console.log(chalk.bgGray(`providers Load Time: ${endTimeProviders - startTimeProviders}ms`));

    // const endInit = performance.now();
    //console.log(chalk.bgGreen(`init NestjsQueryGraphQLModule Load Time: ${endInit - startInit}ms`));
    return {
      module: NestjsQueryGraphQLModule,
      imports: [...opts.imports, coreModule],
      providers: [...providers, ...resolverProviders, pubSubProvider],
      exports: [...providers, ...resolverProviders, ...opts.imports, coreModule, pubSubProvider],
    };
  }

  static defaultPubSubProvider(): Provider<GraphQLPubSub> {
    return { provide: pubSubToken(), useValue: defaultPubSub() };
  }
}
