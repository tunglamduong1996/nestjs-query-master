import { Field, Float, GraphQLISODateTime, ID, ObjectType, registerEnumType } from '@nestjs/graphql'
import { NestjsQueryGraphQLModule } from '../src'
import { FilterableField } from '../src/decorators/filterable-field.decorator'
// import { performance } from 'perf_hooks'
// import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm'
import { Index, Column } from 'typeorm'

export enum BooleanType {
  False = 0,
  True = 1
}

registerEnumType(BooleanType, {
  name: 'BooleanType'
})

export enum DiscountLimitation {
  Unlimited = 0,
  NTimes = 1,
  NTimesPerUser = 2
}

registerEnumType(DiscountLimitation, {
  name: 'DiscountLimitation'
})

export enum DiscountType {
  All = 0,
  Catalog = 1,
  Service = 2,
  Manufacturer = 3
}

registerEnumType(DiscountType, {
  name: 'DiscountType'
})

export enum PriorityType {
  Low = 0,
  Medium = 1,
  High = 2
}

registerEnumType(PriorityType, {
  name: 'PriorityType'
})

export enum CouponStatus {
  Active = 0,
  Locked = 1,
  Deleted = 2
}

registerEnumType(CouponStatus, {
  name: 'CouponStatus'
})

describe('NestjsQueryGraphQLModule', () => {
  @ObjectType('Coupon')
  class Coupon {
    @Field(() => ID)
    id!: string

    @Field(() => String)
    @Column({ name: 'NAME' })
    @Index()
    name!: string

    @Field(() => String, { nullable: true })
    @Column({ name: 'DESCRIPTION' })
    description!: string

    @Field(() => BooleanType, { nullable: true })
    @Column({ name: 'REQUIRE_COUPON_CODE' })
    requireCouponCode!: BooleanType

    @Field(() => String, { nullable: true })
    @Column({ name: 'COUPON_CODE', nullable: true })
    couponCode?: string

    @Field(() => GraphQLISODateTime, { nullable: true })
    @Column({ name: 'START_AT', type: 'timestamp', nullable: true })
    @Index()
    startAt?: Date

    @Field(() => GraphQLISODateTime, { nullable: true })
    @Column({ name: 'END_AT', type: 'timestamp', nullable: true })
    endAt?: Date

    @Field(() => DiscountType, { nullable: true })
    @Column({ name: 'DISCOUNT_TYPE' })
    discountType!: DiscountType

    @Field(() => Float, { nullable: true })
    @Column({ name: 'DISCOUNT_PERCENTAGE' })
    discountPercentage!: number

    @Field(() => BooleanType, { defaultValue: BooleanType.False, nullable: true })
    @Column({ name: 'USE_PERCENTAGE' })
    @Index()
    usePercentage!: BooleanType

    @Field(() => Float, { nullable: true })
    @Column({ name: 'DISCOUNT_AMOUNT' })
    @Index()
    discountAmount!: number

    @Field(() => Float, { nullable: true })
    @Column({ name: 'MAXIMUM_DISCOUNT_AMOUNT' })
    @Index()
    maximumDiscountAmount?: number

    @Field(() => BooleanType, { defaultValue: BooleanType.False, nullable: true })
    @Column({ name: 'IS_CUMULATIVE' })
    @Index()
    isCumulative!: BooleanType

    @Field(() => DiscountLimitation, { nullable: true })
    @Column({ name: 'DISCOUNT_LIMITATION' })
    @Index()
    discountLimitation!: DiscountLimitation

    @Field(() => Float, { nullable: true })
    @Column({ name: 'LIMITATION_TIMES' })
    @Index()
    limitationTimes!: number

    @Field(() => String, { nullable: true })
    @Column({ name: 'DISCOUNT_REQUIREMENT_ID', nullable: true })
    @Index()
    discountRequirementId?: string

    @Field(() => PriorityType, { nullable: true })
    @Column({ name: 'PRIORITY' })
    @Index()
    priority!: PriorityType

    @Field(() => CouponStatus, { nullable: true })
    @Column({ name: 'STATUS' })
    @Index()
    status!: CouponStatus

    @Field(() => BooleanType, { defaultValue: BooleanType.True })
    @Column({ name: 'CALCULATE_IN_TOTAL' })
    @Index()
    calculateInTotal!: BooleanType
  }

  @ObjectType()
  class TestDTO {
    @FilterableField(() => String)
    name!: string

    @FilterableField(() => String, { nullable: true })
    description?: string

    @FilterableField(() => BooleanType, { name: 'BooleanType', })
    requireCouponCode!: BooleanType

    @FilterableField(() => String, { nullable: true })
    couponCode?: string

    @FilterableField(() => GraphQLISODateTime, { nullable: true })
    startAt?: Date

    @FilterableField(() => GraphQLISODateTime, { nullable: true })
    endAt?: Date

    @FilterableField(() => DiscountType, { name: 'DiscountType', })
    discountType!: DiscountType

    @FilterableField(() => BooleanType, { name: 'BooleanType', defaultValue: BooleanType.False })
    usePercentage!: BooleanType

    @FilterableField(() => Float)
    discountPercentage!: number

    @FilterableField(() => Float)
    discountAmount!: number

    @FilterableField(() => Float, { nullable: true })
    maximumDiscountAmount?: number

    @FilterableField(() => BooleanType, { name: 'BooleanType', defaultValue: BooleanType.False })
    isCumulative!: BooleanType

    @FilterableField(() => DiscountLimitation, { name: 'DiscountLimitation', })
    discountLimitation!: DiscountLimitation

    @FilterableField(() => Float)
    limitationTimes!: number

    @FilterableField(() => ID, { nullable: true })
    discountRequirementId?: string

    @FilterableField(() => PriorityType, { name: 'PriorityType', })
    priority!: PriorityType

    @FilterableField(() => CouponStatus, { name: 'CouponStatus', })
    status!: CouponStatus

    @FilterableField(() => BooleanType, { name: 'BooleanType', defaultValue: BooleanType.False })
    calculateInTotal!: BooleanType
  }

  it('should create a module', () => {
    // const startTime = performance.now()
    const graphqlModule = NestjsQueryGraphQLModule.forFeature({
      imports: [
        // NestjsQueryTypeOrmModule.forFeature([
        //   Coupon,
        // ]),
      ],
      resolvers: [
        {
          DTOClass: TestDTO,
          EntityClass: Coupon,
        },
      ],
    })
    // const endTime = performance.now()
    //console.log('NestjsQueryGraphQLModule..........................', endTime - startTime)

    expect(graphqlModule.imports).toHaveLength(2)
    expect(graphqlModule.module).toBe(NestjsQueryGraphQLModule)
    expect(graphqlModule.providers).toHaveLength(3)
    expect(graphqlModule.exports).toHaveLength(5)
  })

  it('should allow a defaultFilter for read options', () => {
    const graphqlModule = NestjsQueryGraphQLModule.forFeature({
      imports: [],
      resolvers: [
        {
          DTOClass: TestDTO,
          EntityClass: TestDTO,
          read: { defaultFilter: { name: { eq: 'foo' } } },
        },
      ],
    })
    expect(graphqlModule.imports).toHaveLength(1)
    expect(graphqlModule.module).toBe(NestjsQueryGraphQLModule)
    expect(graphqlModule.providers).toHaveLength(3)
    expect(graphqlModule.exports).toHaveLength(4)
  })
})
